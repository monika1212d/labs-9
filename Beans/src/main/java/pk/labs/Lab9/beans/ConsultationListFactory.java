package pk.labs.Lab9.beans;

//import java.beans.PropertyChangeListener;
//import java.beans.PropertyVetoException;

/**
 *
 * @author mikus
 */
public interface ConsultationListFactory {
    
    ConsultationList create();
    ConsultationList create(boolean deserialize);
    void save(ConsultationList consultationList);
}
