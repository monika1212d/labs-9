package pk.labs.Lab9.beans.impl;

//import java.io.Serializable;
import java.beans.*;
import java.io.*;
import java.util.*;

import pk.labs.Lab9.beans.*;

public class ListaKonsultacji implements ConsultationList, VetoableChangeListener, Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<Consultation> consultation;
    private Date minDate;
    private Date maxDate;
    //private int minDuration;
    //private int maxDuration;
    private final PropertyChangeSupport p = new PropertyChangeSupport(this);
    private final VetoableChangeSupport vetoes = new VetoableChangeSupport(this);

    public ListaKonsultacji() {
        consultation = new ArrayList();
     
    }

   // private void lista() {
   //     vetoes.addVetoableChangeListener(this);
   // }

   

	@Override
    public int getSize() {
        return consultation.size();
    }

    public void setConsultation(Consultation[] lista) {
        consultation.clear();
        consultation.addAll(Arrays.asList(lista));
    }
    
    @Override
    public Consultation[] getConsultation() {
        Consultation[] a = new Consultation[0];
        return consultation.toArray(a);
    }

    @Override
    public Consultation getConsultation(int index) {
        return consultation.get(index);
    }

    @Override
    public void addConsultation(Consultation consultation) throws PropertyVetoException {
        vetoes.fireVetoableChange(null, null, consultation);
        Consultation[] staraLista = new Consultation[0];
        this.consultation.toArray(staraLista);
        ((Konsultacja) consultation).addVetoableChangeListener(this);
        this.consultation.add(consultation);
        p.firePropertyChange("consultation", staraLista, this.consultation.toArray(staraLista));
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        p.removePropertyChangeListener(listener);
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        p.addPropertyChangeListener(listener);
    }

    public void addVetoableChangeListener(VetoableChangeListener v) {
       vetoes.addVetoableChangeListener(v);
   }

    public void removeVetoableChangeListener(VetoableChangeListener v) {
        vetoes.removeVetoableChangeListener(v);
    }
/*
    public Date getMinDate() {
        return minDate;
    }

    public Date getMaxDate() {
        return maxDate;
    }

   public int getMinDuration() {
        return minDuration;
    }

    public int getMaxDuration() {
        return maxDuration;
    }

    public void setMinDate(Date minDate) {
        this.minDate = minDate;
    }

    public void setMaxDate(Date maxDate) {
        this.maxDate = maxDate;
    }

    public void setMinDuration(int minDuration) {
        this.minDuration = minDuration;
    }

    public void setMaxDuration(int maxDuration) {
        this.maxDuration = maxDuration;
    }
*/
    @Override
    public void vetoableChange(PropertyChangeEvent e) throws PropertyVetoException {
        Consultation con = (Konsultacja) e.getNewValue();
        if ((minDate != null && con.getBeginDate().before(minDate))
                || (maxDate != null && con.getEndDate().after(maxDate))) {
            throw new PropertyVetoException("", e);
        }
        if (e.getPropertyName() == "duration") {
            for (Consultation cons : consultation) {
                if (con.getEndDate().after(cons.getBeginDate()) && con.getEndDate().before(cons.getEndDate())
                        || (con.getBeginDate().before(cons.getBeginDate()) && con.getEndDate().after(cons.getEndDate()))) {
                    throw new PropertyVetoException("", e);
                }
            }
        } else {
            for (Consultation cons : consultation) {
                if ((con.getBeginDate().after(cons.getBeginDate()) && con.getBeginDate().before(cons.getEndDate()))
                        || (con.getEndDate().after(cons.getBeginDate()) && con.getEndDate().before(cons.getEndDate()))
                        || (con.getBeginDate().before(cons.getBeginDate()) && con.getEndDate().after(cons.getEndDate()))
                        || con.getBeginDate().equals(cons.getBeginDate()) || con.getEndDate().equals(cons.getEndDate())) {
                    throw new PropertyVetoException("", e);
                }
            }
        }
    }
}