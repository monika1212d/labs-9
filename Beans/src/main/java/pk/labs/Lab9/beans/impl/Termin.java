package pk.labs.Lab9.beans.impl;
import java.io.Serializable;
import java.util.*;
import pk.labs.Lab9.beans.*;

public class Termin implements Term, Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Date begin;
    private int duration;
	
	  public Termin()
    {
        begin=new Date();
    }
    
    public Termin(Date begin, int duration){
        this.begin=begin;
        this.duration=duration;
    }

    @Override
    public Date getBegin() {
        return begin;
    }

    @Override
    public void setBegin(Date begin) {
        this.begin=begin;
    }

    @Override
    public int getDuration() {
        return duration;
    }

    @Override
    public void setDuration(int duration) {
        if(duration>0){
            this.duration=duration;
        }
    }
 @Override
    public Date getEnd() {
        Calendar cal= Calendar.getInstance();
        cal.setTime(begin);
        cal.add(Calendar.MINUTE,duration);
        return cal.getTime();
    }
}
