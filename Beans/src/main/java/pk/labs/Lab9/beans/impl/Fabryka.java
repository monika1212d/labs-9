//package pk.labs.Lab9.beans;
package pk.labs.Lab9.beans.impl;
//import java.beans.*;

import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

//import java.io.*;
//import java.util.*;
import pk.labs.Lab9.beans.*;

public class Fabryka implements ConsultationListFactory, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Override

    public void save(ConsultationList consultationList) {
        try 
            {
            XMLEncoder enc = new XMLEncoder(new BufferedOutputStream(new FileOutputStream("lab.xml")));
                enc.writeObject(consultationList);
                enc.close();
            }
            catch (IOException e) {} 
    }

    @Override
    public ConsultationList create(boolean deserialize) {
        ListaKonsultacji consultationList = new ListaKonsultacji();
        if (deserialize) {
            XMLDecoder dec;
            try {

                dec = new XMLDecoder(new BufferedInputStream(new FileInputStream("lab.xml")));
                consultationList = (ListaKonsultacji) dec.readObject();
                dec.close();
            } catch (FileNotFoundException ex) {
                Logger.getLogger(Fabryka.class.getName()).log(Level.SEVERE, null, ex);
            }

            return consultationList;

        } else {
            return this.create();
        }
    }

    @Override
    public ConsultationList create() {
        ListaKonsultacji lista = new ListaKonsultacji();
        return lista;
    }

}
