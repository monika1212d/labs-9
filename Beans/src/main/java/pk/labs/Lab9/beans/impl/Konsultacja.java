package	pk.labs.Lab9.beans.impl;
import java.beans.*;
import java.io.Serializable;
import java.util.*;

import pk.labs.Lab9.beans.*;

public class Konsultacja implements Consultation, Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Termin term;
	private String student;
	private final VetoableChangeSupport vetoes = new VetoableChangeSupport(this);
	
	public Konsultacja(){
	term = new Termin();
	}
	
	public Konsultacja(Termin term, String student){
	this.term = term;
	this.student = student;
	}
	@Override
	public Date getBeginDate(){
	return term.getBegin();
	}
	@Override
	public Date getEndDate(){
	return term.getEnd();
	}
	@Override
	public void prolong(int minutes) throws PropertyVetoException{
		int newDuration;
	if (minutes<=0){
		
	}else{
	newDuration = term.getDuration();
        term.setDuration(newDuration + minutes);
	vetoes.fireVetoableChange("duration", newDuration,  newDuration + minutes);
	}
	}
	@Override
	public String getStudent(){
	return student;
	}
	 public Term getTerm() {
        return term;
    }
	  public void addVetoableChangeListener(VetoableChangeListener v) {
        vetoes.addVetoableChangeListener(v);
    }

    public void removeVetoableChangeListener(VetoableChangeListener v) {
        vetoes.removeVetoableChangeListener(v);
    }

	@Override
	public void setStudent(String student) {
		this.student=student;
	}

	@Override
	public void setTerm(Term term) throws PropertyVetoException {
                Termin t = this.term;
                this.term=(Termin) term;
                vetoes.fireVetoableChange("termin", t, term);
	}
}
		