package pk.labs.Lab9;
//package pk.labs.Lab9.beans.impl;

import pk.labs.Lab9.beans.Consultation;
import pk.labs.Lab9.beans.ConsultationList;
import pk.labs.Lab9.beans.ConsultationListFactory;
import pk.labs.Lab9.beans.Term;
import pk.labs.Lab9.beans.impl.*;

public class LabDescriptor {
    
    public static Class <? extends Term> termBean = Termin.class;
    public static Class <? extends Consultation> consultationBean = Konsultacja.class;
    public static Class <? extends ConsultationList> consultationListBean = ListaKonsultacji.class;
    public static Class <? extends ConsultationListFactory> consultationListFactory = Fabryka.class;
    
}
